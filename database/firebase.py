import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore as firestore_admin
from pyrebase import pyrebase
from pyrebase.pyrebase import Auth

from config import firebase_config,SERVICE_CREDENTIALS_PATH

firebase = pyrebase.initialize_app(firebase_config)
pyrebase_auth: Auth = firebase.auth()

cred = credentials.Certificate(SERVICE_CREDENTIALS_PATH)
firebase_admin.initialize_app(cred, firebase_config)

firestore = firestore_admin.client()

