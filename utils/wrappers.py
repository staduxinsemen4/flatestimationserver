import json

import flask_restful
import requests

from arguments_parser.login import login_parser
from database import pyrebase_auth,firestore


def auth_required(role: str):
    def wrap(func):
        parser = login_parser()
        parser_args = parser.parse_args()
        try:
            users_info = pyrebase_auth.get_account_info(parser_args.get("token"))
            user = users_info.get("users")[0]
            local_id = user.get("localId")
            user_snapshot = firestore.collection("users").document(local_id).get().to_dict()
            user_roles = user_snapshot.get("roles")
            if not user_roles:
                return flask_restful.abort(401,
                                           message=f"to perfom this, you need to {role} role enabled,"
                                                   f"but your roles is undefined")
            user_roles = dict(user_roles)
            if user_roles.get(role,False):
                return func
            else:
                return flask_restful.abort(401,
                                           message=f"to perfom this, you need to {role} role enabled,"
                                                   f"but your roles is {user_roles}")

        except requests.exceptions.HTTPError as e:
            return flask_restful.abort(401, err=json.loads(e.strerror))

    return wrap
