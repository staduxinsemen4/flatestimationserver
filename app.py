from flask import Flask
from flask_restful import Api

from resources import FlatEstimation, Login
from resources.check_flat import CheckFlat
from resources.refresh_token import RefreshToken
from regular_tasks import init_regular_tasks
app = Flask(__name__)
api = Api(app)

api.add_resource(FlatEstimation, "/flat_estimation")
api.add_resource(CheckFlat, "/flat_estimation/check_flat")
api.add_resource(Login, "/login")
api.add_resource(RefreshToken, "/refresh_token")

tasks = init_regular_tasks()
if __name__ == "__main__":
    tasks.start(block=False)
    app.run("127.0.0.1", port=5000, debug=True)
