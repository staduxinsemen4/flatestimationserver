from datetime import datetime

from flask_restful import Resource

from arguments_parser.check_flat import update_parser
from database import firestore
from utils.wrappers import auth_required


class CheckFlat(Resource):
    def __init__(self):
        self.update_parser = update_parser()

    method_decorators = {'patch': [auth_required(role="writer")]}

    def patch(self):
        args = self.update_parser.parse_args()
        ref = firestore.collection("flats_data")
        flat_id = args.pop("id", '')
        platform = args.get("platform", '')
        if flat_id and platform:
            flat_snapshot = ref.where("id", '==', flat_id).where("platform",'==',platform).get()[0]
            last_checked = datetime.utcnow().timestamp()
            ref.document(flat_snapshot.id).update(dict(last_checked=last_checked))
            print('jju')
            print({f"{flat_id}_{platform}": last_checked})
            return {f"{flat_id}_{platform}": last_checked}
