import json

import requests
from flask_restful import Resource

from arguments_parser.refresh_token import get_parser
from database import pyrebase_auth


class RefreshToken(Resource):
    def get(self):
        parser = get_parser()
        args = parser.parse_args()
        try:
            refresh_token = args.get("refreshToken")
            new_tokens = pyrebase_auth.refresh(refresh_token)

        except requests.exceptions.HTTPError as e:
            return json.loads(e.strerror)
        else:
            return {"token": new_tokens.get("idToken"),
                    "refreshToken": new_tokens.get("refreshToken")}
