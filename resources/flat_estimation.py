from datetime import datetime

from flask_restful import Resource

from arguments_parser.flat_estimation import put_parser, get_parser
from database import firestore
from utils.wrappers import auth_required


class FlatEstimation(Resource):
    def __init__(self):
        self.put_parser = put_parser()
        self.get_parser = get_parser()

    method_decorators = {'get': [auth_required(role="reader")],
                         "put": [auth_required(role="writer")]}

    def get(self):
        args = self.get_parser.parse_args()
        sort_by = args.pop("sort_by", '')
        query = get_query(args)

        flats = [flat.to_dict() for flat in query.stream()]
        if sort_by:
            flats = [flat_data for flat_data in
                     sorted(flats, key=lambda flat: flat[sort_by],reverse=True)]
        print({"flats":flats})
        return {"flats":flats}

    def put(self):
        args = self.put_parser.parse_args()
        flat_name = f"{args.get('platform')}_{args.get('city')}_{args.get('type')}_{args.get('rating')}_{args.get('id')}"
        firestore.collection("flats_data").document(flat_name).set(
            dict(**args, last_checked=datetime.utcnow().timestamp()))

        return {"flat_name": flat_name}


def get_query(args: dict):
    query = None
    ref = firestore.collection("flats_data")
    flat_id = args.pop("flat_id", '')
    platform = args.get("platform", '')
    if flat_id and platform:
        return ref.where("id", '==', flat_id).where("platform", "==", platform)
    print(args)
    for arg_name, arg_value in args.items():
        if not arg_value: continue
        query_args = (arg_name, "==", arg_value)
        print(query_args)
        if query:

            query = query.where(*query_args)
        else:
            query = ref.where(*query_args)

    return query
