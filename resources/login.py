import requests
from flask_restful import Resource

from arguments_parser.login import get_parser
from database import pyrebase_auth
import json

class Login(Resource):

    def get(self):
        parser = get_parser()
        args = parser.parse_args()
        try:
            user = pyrebase_auth.sign_in_with_email_and_password(email=args.get("email"),password=args.get("password"))

        except requests.exceptions.HTTPError as e:
            return json.loads(e.strerror)
        else:
            return {"token": user.get('idToken'),
                    "refreshToken": user.get('refreshToken'),
                    "expiresInSeconds": user.get('expiresIn')}