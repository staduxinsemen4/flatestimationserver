from datetime import datetime

from timeloop import Timeloop

from config import FLATS_OUTDATED_TASK_INTERVAL, FLATS_OUTDATED_LIMIT
from database import firestore


def init_regular_tasks():
    tl = Timeloop()

    @tl.job(interval=FLATS_OUTDATED_TASK_INTERVAL)
    def delete_outdated_flats():
        delete_time = (datetime.utcnow() - FLATS_OUTDATED_LIMIT).timestamp()
        print(datetime.utcnow().timestamp())
        ref = firestore.collection("flats_data")
        for flat_snapshot in ref.where("last_checked", "<=", delete_time).stream():
            flat_reference = flat_snapshot.reference
            flat_reference.delete()
            print(flat_reference.id + " was deleted")

    return tl
