from flask_restful.reqparse import RequestParser


def get_parser() -> RequestParser:
    parser = RequestParser()
    parser.add_argument("email", required=True,location = ['json','form'])
    parser.add_argument("password", required=True,location = ['json','form'])
    return parser

def login_parser() -> RequestParser:
    parser = RequestParser()
    parser.add_argument("token", required=True,location = 'headers')
    return parser