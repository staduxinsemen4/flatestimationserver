from flask_restful.reqparse import RequestParser


def get_parser() -> RequestParser:
    parser = RequestParser()
    parser.add_argument("refreshToken", required=True,location = 'headers')
    return parser
