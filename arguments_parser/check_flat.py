from flask_restful.reqparse import RequestParser


def update_parser() -> RequestParser:
    parser = RequestParser()

    parser.add_argument("id", type=str, location="args", required=True)
    parser.add_argument("platform", type=str, location="args", required=True)
    return parser
