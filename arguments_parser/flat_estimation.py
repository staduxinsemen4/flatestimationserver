from flask_restful.reqparse import RequestParser


def put_parser() -> RequestParser:
    parser = RequestParser()

    parser.add_argument('platform', type=str, location="json")
    parser.add_argument('city', type=str, location="json")
    parser.add_argument('owner', type=str, location="json")
    parser.add_argument("type", type=str, location="json")
    parser.add_argument('link', type=str, location="json")
    parser.add_argument('id', type=str, location="json")
    parser.add_argument('price', type=int, location="json")
    parser.add_argument('deposit', type=int, location="json")
    parser.add_argument('images', type=list, location="json")
    parser.add_argument('rating', type=int, location="json")
    parser.add_argument('district', type=str, location="json")
    parser.add_argument('commission', type=int, location="json")
    parser.add_argument("address",type=str,location="json")
    parser.add_argument("description", type=str, location="json")
    parser.add_argument("rooms_number", type=int, location="json")
    parser.add_argument("floor", type=int, location="json")
    parser.add_argument("max_floor", type=int, location="json")
    parser.add_argument("house_area", type=float, location="json")
    return parser


def get_parser() -> RequestParser:
    parser = RequestParser()

    parser.add_argument("id", type=str, location="args")
    parser.add_argument("city", type=str, location="args")
    parser.add_argument("type", type=str, location="args")
    parser.add_argument("district", type=str, location="args")

    parser.add_argument("sort_by", type=str, location="args")

    return parser
